<?php

namespace Drupal\commerce_winbank_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Endpoints for the routes defined.
 */
class CallbackController extends ControllerBase {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger_factory->get('commerce_winbank_redirect');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Callback action.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function callback(Request $request) {
    $action = $request->get("action");

    if ($action == NULL) {      
      return $this->createRedirectForm($request);
    }

    if ($action === 'language_redirect') {      
      $tempstore = \Drupal::service('user.private_tempstore')->get('commerce_winbank_redirect');
      $message = $tempstore->get('winbank_message');
      return [
        '#type' => 'markup',
        '#markup' => t($message),
        '#cache' => [
          'max-age' => 5,
        ],
      ];
    }

    $message = $this->processCallback($request);

    try {
      $language = NULL;
      $parameters = $request->get('Parameters');
      $params = [];
      $values = explode(';', $parameters);
      foreach($values as $kv) {
        list($k, $v) = explode(':', $kv);
        $params[ $k ] = $v;  
      }
      $language = $params['lang'];

    } catch (Exception $e) {} 

    // See if you need to redirect to appropriate language
    $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($language !== NULL && $lang_code !== $language) {
      $current_path = \Drupal::service('path.current')->getPath();
      $path = "/" . $language . $current_path . '?action=language_redirect';
      $mess = $message->render();
      $tempstore = \Drupal::service('user.private_tempstore')->get('commerce_winbank_redirect');
      $tempstore->set('winbank_message', $mess);
      return new RedirectResponse($path);
    }

    return [
      '#type' => 'markup',
      '#markup' => $message,
      '#cache' => [
        'max-age' => 5,
      ],
    ];
  }

  public function createRedirectForm(Request $request) {
    $form = \Drupal::formBuilder()->getForm('Drupal\commerce_winbank_redirect\Form\StaticRedirectForm');
    $exclude_keys = ["op", "form_id", "form_token", "form_build_id", "PaymentUrl"];
    foreach ($request->request->all() as $key => $value) {
      if (in_array($key, $exclude_keys)) {
        continue;
      }
      $form[$key] = [
        '#name' =>  $key,     
        '#value' => $value,    
        '#type' => 'hidden',
      ];  
    }

    $form['#action'] = $request->get('PaymentUrl');
    $form['#attributes']['class'][] = 'autosubmit-form';
    $form['#attached']['library'][] = 'commerce_winbank_redirect/autosubmit-form';

    $render = [];
    $render['form'] = $form;
    return $render;
  }

  /**
   * Process the callback from winbank.
   */
  public function processCallback(Request $request) {
    $results = $request->request->all();
    $this->logger->notice(json_encode($results));

    $action = $request->get("action");
    if ($action == "cancel") {
      return t("You have cancelled your payment.");
    }

    $resultCode = $request->get('ResultCode');
    if ($resultCode !== "0") {
      $this->logResponse($request, "failure");
      return $this->getDisplayMessage($request);
    }

    $hashkey = $request->get('HashKey');
    $order_id = $request->get('MerchantReference');
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    $ticket_result = $order->getData("IssueNewTicketResult");

    $hash = $this->calculateHash($request, $order_id, $ticket_result);
    if (strtolower($hash) !== strtolower($hashkey)) {
      $this->createPayment($order, $request, $ticket_result, "Unvalidated");
      $this->logResponse($request, "failure");
      return t("Sorry we were not able to validate your payment.");
    }

    $this->createPayment($order, $request, $ticket_result);
    $this->logResponse($request, "success");
    return $this->getDisplayMessage($request);
  }

  /**
   * Log response data to database log and order object.
   */
  public function logResponse($request, $status = "success") {
    $order_id = $request->get('MerchantReference');
    $data = [
      "SupportReferenceID" => $request->get('SupportReferenceID'),
      "MerchantReference" => $order_id,
      "ResponseCode" => $request->get('ResponseCode'),
      "ResponseDescription" => $request->get('ResponseDescription'),
    ];

    if($status === "success"){
      $data2 = [
        "StatusFlag" => $request->get('StatusFlag'),
        "ApprovalCode" => $request->get('ApprovalCode'),
        "PackageNo" => $request->get('PackageNo'),
        "AuthStatus" => $request->get('AuthStatus'),
      ];
      $data = array_merge($data,$data2);
    }

    // log for immediate action
    if($status === "success"){
      $this->logger->info("Payment " . $status . ": " . '<pre><code>' . print_r($data, TRUE) . '</code></pre>');
    }else if($status === "failure"){
      $this->logger->info("Payment " . $status . ": " . '<pre><code>' . print_r($data, TRUE) . '</code></pre>');
    }

    // log in the order data field for future reference
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    $IssueNewTicketResult = $order->getData("IssueNewTicketResult");
    $IssueNewTicketResult = array_merge($IssueNewTicketResult, $data);
    $order->setData("IssueNewTicketResult", $IssueNewTicketResult);
    $order->save();
  }

  /**
   * Create Payment.
   */
  public function createPayment($order, $request, $ticket_result, $state = "completed") {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => $state,
      'amount' => $order->getBalance(),
      'payment_gateway' => $ticket_result["payment_gateway"],
      'order_id' => $order->id(),
      'remote_id' => $request->get('TransactionId'),
      'remote_state' => $request->get('StatusFlag'),
    ]);
    if ($state == "completed") {
      $payment->setAuthorizedTime(REQUEST_TIME);
      $payment->setCompletedTime(REQUEST_TIME);
    }
    $payment->save();
  }

  /**
   * Calculates hash key by concatenation of values, then uses sha256 algorithm.
   *
   * @param $request
   *
   * @param $order_id
   *
   * @param $IssueNewTicketResult
   *
   * @param $ticket_result
   *
   * @return string
   */
  public function calculateHash($request, $order_id, $ticket_result) {
    $concatValues = sprintf(
        '%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s',
        $ticket_result['TranTicket'],
        $ticket_result['PosId'],
        $ticket_result['AcquirerId'],
        $order_id,
        $request->get('ApprovalCode'),
        $request->get('Parameters'),
        $request->get('ResponseCode'),
        $request->get('SupportReferenceID'),
        $request->get('AuthStatus'),
        $request->get('PackageNo'),
        $request->get('StatusFlag')
    );
    return hash_hmac('sha256', $concatValues, $ticket_result['TranTicket'], FALSE);
  }

  /**
   * Get message from request results
   *
   * @param $request
   * 
   * @return string
   */
  public function getDisplayMessage($request) {
    $message = t('Transaction not complete.');
    $resultCode = $request->get('ResultCode');
    $resultCodes = [
      '1048' => t('Transaction not complete. The transaction has previously been processed.'),
      '500' => t('Transaction not complete because of technical problem. Please try again later.'),
      '981' => t('Transaction not complete. Card details not correct or bank does not support type of card.'),
      '1045' => t('Transaction not complete. The transaction is probably still in process.'),
      '1072' => t('Transaction not complete because of technical problem. Please try again later.'),
      '1' => t('Transaction not complete because of technical problem. Please try again later.'),
    ];
    if (isset($resultCodes[$resultCode])) {
      $message = $resultCodes[$resultCode];
    } elseif ($resultCode === '0') {
      $responseCode = $request->get('ResponseCode');
      if ($responseCode == '00') {
        $message = t('Your transaction was completed. Thank you.');
      } elseif ($responseCode == '12') {
        $message = t('Your transaction was denied by the issuing bank. Can retry with a different card or contact support.');
      }
    }
    return $message;
  }

}
