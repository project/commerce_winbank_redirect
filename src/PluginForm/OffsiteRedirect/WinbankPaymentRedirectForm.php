<?php

namespace Drupal\commerce_winbank_redirect\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 *
 */
class WinbankPaymentRedirectForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get('commerce_winbank_redirect');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $payment_gateway_plugin->getConfiguration();
    $order = $payment->getOrder();
    // Format amount.
    $amount = sprintf('%0.2f', $order->getTotalPrice()->getNumber());

    if ($config['mode'] == 'test') {
      $config["tickets_url"] = $config["tickets_url_test"];
      $config["paycenter_url"] = $config["paycenter_url_test"];
    }

    // Validate merchant_id, other validations ?
    if (empty($config['merchant_id'])) {
      throw new PaymentGatewayException('Merchant ID not provided.');
    }

    // Get new ticket.
    try {
      $result = $this->getNewTicket($amount, $order->id(), $config);

      if ($result !== null && $result->IssueNewTicketResult->ResultCode == 0) {

        // Save response data for validating response later.
        $order->setData("IssueNewTicketResult", [
          "ResultCode" => $result->IssueNewTicketResult->ResultCode,
          "ResultDescription" => $result->IssueNewTicketResult->ResultDescription,
          "TranTicket" => $result->IssueNewTicketResult->TranTicket,
          "Timestamp" => $result->IssueNewTicketResult->Timestamp,
          "MinutesToExpiration" => $result->IssueNewTicketResult->MinutesToExpiration,
          "PosId" => $config["pos_id"],
          "AcquirerId" => $config["acquirer_id"],
          "Parameters" => '',
          "payment_gateway" => $payment->getPaymentGateway()->id(),
        ]);
        $order->save();

      }
      else {
        $message = 'There was a problem connecting ot the ticket issuer.';
        if(isset($result->IssueNewTicketResult->ResultDescription)){
          $message .= " " . $result->IssueNewTicketResult->ResultDescription;
        }
        throw new InvalidResponseException($message);
      }
    }
    catch (Exception $e) {
      $message = 'Could not connect to bank ticket issuer';
      throw new InvalidResponseException($message);
    }

    // Prepare redirect form.
    $data = [
      'AcquirerId' => $config['acquirer_id'],
      'MerchantId' => $config['merchant_id'],
      'PosId' => $config['pos_id'],
      'User' => $config['username'],
      'MerchantReference' => $order->id(),
      'LanguageCode' => $this->getLanguageCode($config['language_code']),
      'ParamBackLink' => '',
      'PaymentUrl' => $config["paycenter_url"],
    ];

    $form = $this->buildRedirectForm($form, $form_state, "/commerce_winbank_redirect/callback", $data, 'post');
    if ($config['auto_redirect'] !== "1") {
      unset($form['#attached']['library']);
    }
    return $form;
  }

  /**
   * Soap request to get NewTicket for transaction.
   *
   * @param int $amount
   *   The order amount.
   * @param int $orderid
   *   The order id.
   * @param int $config
   *   The configuration object.
   *
   * @return array
   *   Object containing the soap service response.
   */
  public function getNewTicket($amount, $orderid, $config) {
    // libxml_disable_entity_loader(false);
    try {
      if ($config['mode'] == 'test') {
        $options = [
          'location'     => $config['tickets_url'],
          'uri'        => $config['tickets_url'],
        ];
        $soap = new \SoapClient(NULL, $options);
      } else {
        $soap = new \SoapClient($config['tickets_url']);
      }
      $ticketRequest = [
        'Username' => $config['username'],
        'Password' => hash('md5', $config['password']),
        'MerchantId' => $config['merchant_id'],
        'PosId' => $config['pos_id'],
        'AcquirerId' => $config['acquirer_id'],
        'MerchantReference' => $orderid,
        'RequestType' => $config['request_type'],
        'ExpirePreauth' => 0,
        'Amount' => $amount,
        'CurrencyCode' => $config['currency_code'],
        'Installments' => 0,
        'Bnpl' => '0',
        'Parameters' => 'lang:' . \Drupal::languageManager()->getCurrentLanguage()->getId(),
      ];
      $xml = [
        'Request' => $ticketRequest,
      ];
      $result = $soap->IssueNewTicket($xml);
      $this->logger->notice(json_encode($result));
      return $result;
    }
    catch (\SoapFault $exception) {
      $message = "Trying to IssueNewTicket on " . $config['tickets_url'] . ". " . $exception->getMessage();
      $this->logger->alert($message);
    }
  }

  /**
   * Get language code for winbank interface
   * If no matching language is found based on current user 
   * default_lang code from settings is returned
   * 
   * @return string
   *   return language code for winbank interface.
   */
  public function getLanguageCode($default_lang) {
    $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $available = [
      'el' => 'el-GR',
      'en' => 'en-US',
      'ru' => 'ru-RU',
      'de' => 'de-DE'
    ];
    if (isset($available[$lang_code]))
      return $available[$lang_code];
    else
      return $default_lang;
  }

}