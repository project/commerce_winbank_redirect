<?php

namespace Drupal\commerce_winbank_redirect\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "winbank_redirect",
 *   label = "Winbank Payment Redirect",
 *   display_label = "Winbank Payment Redirect",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_winbank_redirect\PluginForm\OffsiteRedirect\WinbankPaymentRedirectForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class WinbankPaymentRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'acquirer_id' => '',
      'pos_id' => '',
      'username' => '',
      'password' => '',
      'tickets_url' => 'https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?WSDL',
      'tickets_url_test' => 'http://localhost:8008/ticket.php',
      'paycenter_url' => 'https://paycenter.piraeusbank.gr/redirection/pay.aspx',
      'paycenter_url_test' => 'http://localhost:8008/paycenter.php',
      'request_type' => '02',
      'currency_code' => '978',
      'language_code' => 'en-US',
      'auto_redirect' => 1,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Winbank (Pireaus) Merchant Id.'),
      '#default_value' => $this->configuration['merchant_id'],
    ];

    $form['acquirer_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('AcquirerId'),
      '#description' => $this->t('Winbank (Pireaus) Merchant Id.'),
      '#default_value' => $this->configuration['acquirer_id'],
    ];

    $form['pos_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('PosId'),
      '#description' => $this->t('Winbank (Pireaus) POS Id.'),
      '#default_value' => $this->configuration['pos_id'],
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Username'),
      '#description' => $this->t('Winbank (Pireaus) Username or user.'),
      '#default_value' => $this->configuration['username'],
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Password'),
      '#description' => $this->t('Winbank (Pireaus) Password.'),
      '#default_value' => $this->configuration['password'],
    ];

    $form['tickets_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Tickets URL'),
      '#description' => $this->t('Winbank (Pireaus) ticketing Web Service URL.'),
      '#default_value' => $this->configuration['tickets_url'],
    ];

    $form['tickets_url_test'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Tickets URL for Test'),
      '#description' => $this->t('Winbank (Pireaus) ticketing Web Service URL for Test.'),
      '#default_value' => $this->configuration['tickets_url_test'],
    ];

    $form['paycenter_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Paycenter URL'),
      '#description' => $this->t('Winbank (Pireaus) Paycenter URL.'),
      '#default_value' => $this->configuration['paycenter_url'],
    ];

    $form['paycenter_url_test'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Paycenter URL For Test'),
      '#description' => $this->t('Winbank (Pireaus) Paycenter URL for Test.'),
      '#default_value' => $this->configuration['paycenter_url_test'],
    ];

    $form['request_type'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('RequestType'),
      '#description' => $this->t('Winbank RequestType, 02 Sale, 00 Preauthorization (not implemented).'),
      '#default_value' => $this->configuration['request_type'],
    ];

    $form['currency_code'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('CurrencyCode'),
      '#description' => $this->t('Winbank (Pireaus) CurrencyCode, 978 Euro.'),
      '#default_value' => $this->configuration['currency_code'],
    ];

    $form['language_code'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('LanguageCode'),
      '#description' => $this->t('Winbank (Pireaus) LanguageCode, en-US, el-GR, ru-RU, de-DE, used as default if no matching language found'),
      '#default_value' => $this->configuration['language_code'],
    ];

    $form['auto_redirect'] = [
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#title' => $this->t('Auto Redirect'),
      '#description' => $this->t('Should the user be automatically redirected to bank ?'),
      '#default_value' => $this->configuration['auto_redirect'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['acquirer_id'] = $values['acquirer_id'];
      $this->configuration['pos_id'] = $values['pos_id'];
      $this->configuration['username'] = $values['username'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['tickets_url'] = $values['tickets_url'];
      $this->configuration['tickets_url_test'] = $values['tickets_url_test'];
      $this->configuration['paycenter_url'] = $values['paycenter_url'];
      $this->configuration['paycenter_url_test'] = $values['paycenter_url_test'];
      $this->configuration['currency_code'] = $values['currency_code'];
      $this->configuration['language_code'] = $values['language_code'];
      $this->configuration['request_type'] = $values['request_type'];
      $this->configuration['auto_redirect'] = $values['auto_redirect'];
    }
  }

}
