/**
 * @file
 * A JavaScript file containing the autosubmit functionality
 */

(function (Drupal, $) {
  Drupal.behaviors.winbankAutosubmitForm = {
    attach: function (context) {
      $(document).ready(function(){
        $('.autosubmit-form').submit();
      });
    }
  }
})(Drupal, jQuery);
