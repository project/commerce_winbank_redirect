<?php

/**
 * @file
 */

if (isset($_REQUEST['domain'])) {
  $domain = $_REQUEST['domain'];
} else {
  $scheme = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_SCHEME);
  $referrer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
  if ($referrer) {
    $domain = $scheme . "://" . $referrer;  
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Winbank Paycenter Mock Form</title>
</head>
<body>
  <div>
    <h1>Received</h1>
<?php
foreach ($_REQUEST as $key => $value) {
  echo $key . " : " . $value . "<br />\r\n";
}
?>

</div>
<h1>Will Send</h1>
<form action="<?php echo $domain ?>/commerce_winbank_redirect/callback" method="post" onsubmit="addStatusAndSubmit(this)">
  SupportReferenceID: <input type="text" name="SupportReferenceID" value="1001"><br/>
  ResultCode: <select name="ResultCode" id="">
    <option selected="selected" value="0">0 Good</option>
    <option value="1">non 0 Failure</option>
  </select><br/>
  ResultDescription: <input type="text" name="ResultDescription" value="Result Description"><br/>
  StatusFlag: <select name="StatusFlag" id="StatusFlag">
    <option selected="selected" value="success">Success</option>
    <option value="failure">Failure</option>
    <option value="cancel">Cancel</option>
  </select><br/>
  ResponseCode: <select name="ResponseCode" id="">
    <option selected="selected" value="00">Approved 00, 08, 10, 16</option>
    <option value="05">Declined</option>
  </select><br/>
  ResponseDescription: <input type="text" name="ResponseDescription" value="Response Description"><br/>
  LanguageCode: <select name="LanguageCode" id="">
    <option value="el-GR">el-GR</option>
    <option selected="selected" value="en-US">en-US</option>
    <option value="ru-RU">ru-RU</option>
    <option value="de-DE">de-DE</option>
  </select><br/>
  MerchantReference: <input type="text" name="MerchantReference" value="<?php echo $_REQUEST["MerchantReference"] ?>"><br/>
  TransactionId: <input type="text" name="TransactionId" value="10001"><br/>
  TransactionDateTime: <input type="text" name="TransactionDateTime" value="22/04/2019 15:33:22"><br/>
  CardType: <select name="CardType" id="">
    <option value="1">Visa</option>
    <option selected="selected" value="2">Mastercard</option>
    <option value="3">Maestro</option>
    <option value="4">American Express</option>
    <option value="5">Diners ή Discover</option>
  </select><br/>
  PackageNo: <input type="text" name="PackageNo" value="1"><br/>
  ApprovalCode: <input type="text" name="ApprovalCode" value="111111"><br/>
  RetrievalRef: <input type="text" name="RetrievalRef" value="1"><br/>
  AuthStatus: <select name="AuthStatus" id="">
    <option selected="selected" value="01">Verified by Visa / Mastercard SecureCode</option>
    <option value="02">Verified by Visa / Mastercard SecureCode</option>
    <option value="03">Anonymous</option>
  </select><br/>
  Parameters: <input type="text" name="Parameters" value=""><br/>
  HashKey: <input type="text" name="HashKey" value="551f158e669965f30bcfa65e558fd4aabb191d394de39be2adfab416575102d7"><br/>
  PaymentMethod: <select name="PaymentMethod" id="">
    <option selected="selected" value="Card">Card</option>
    <option value="MasterPass">MasterPass</option>
    <option value="MyBank">MyBank</option>
  </select><br/>
  <input type="submit">
</form>
<script>
  function addStatusAndSubmit(form) {
    var st = document.getElementById("StatusFlag");
    var status = st.options[st.selectedIndex].value;
    console.log(status);
    form.action = form.action + "?action=" + status;
    //form.submit();
    return false;
  }
</script>
</body>
</html>
