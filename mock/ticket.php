<?php

/**
 *
 */
class TicketSoapServer {

  /**
   *
   */
  public function IssueNewTicket($xml) {
    $result = [];
    $IssueNewTicketResult = (object) [
      "ResultCode" => "0",
      "TranTicket" => "123",
      "Timestamp" => time(),
      "MinutesToExpiration" => 15,
      "ResultDescription" => "Result description",
    ];
    return (object) ["IssueNewTicketResult" => $IssueNewTicketResult];
  }

}
$options = ['uri' => 'http://localhost:8008/ticket.php'];
$server = new SoapServer(NULL, $options);
$server->setClass('TicketSoapServer');
$server->handle();
