CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Testing connection
 * Local mock server
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 Provides payment gateway for Drupal 8 commerce. It is a redirection for Winbank (Piraeus Bank).  Winbank is a banking institute with physical points in many areas of the world.

By using redirection payments your clients get redirect to the bank and fill in their credit card data there. No information is stored at the Drupal side.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_winbank_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_winbank_redirect

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * Go to `Commerce > configuration > payment > payment gateways` and add a new method. Choose Winbank from the options provided and fill in the required fields which will be given with the bank aggreement.

TESTING CONNECTION
---------------

Bank should provide testing endpoints as soon as the bank contract aggreement is signed.  You will be asked to provide the following to the bank:

```
Web site URL: https://yourdomain.com
Referrer URL: https://yourdomain.com/commerce_winbank_redirect/callback
Success URL: https://yourdomain.com/commerce_winbank_redirect/callback?action=success
Failure URL: https://yourdomain.com/commerce_winbank_redirect/callback?action=failure
Backlink URL: https://yourdomain.com/commerce_winbank_redirect/callback?action=cancel
IP address: x.x.x.x
Answer method: POST
Installments: No (Currently not implemented)
Payment method: to appear or not appear (check manual)
```

LOCAL MOCK SERVER
---------------

The mock server can't replace the actual testing environment that the bank provides.  The development of this module has started before access was given so it served as a quick testing ground.  It is kept as part of the project as it can be used for debugging or understanding the code locally.

Start the mock server and if the payment is configured to test (not live) it will communicate with the local mock server

```
cd web/modules/contrib/commerce_winbank_redirect
npm run mock
```

The mock urls can be changed from the configuration settings.


TROUBLESHOOTING
---------------

 * Use the mock server locally for any live debugging if no direct connection with the bank endpoints is not available.
 * Post in the issue queue if you still have trouble

MAINTAINERS
-----------

Current maintainers:

 * Giorgos Kontopoulos (https://www.drupal.org/user/62448)
